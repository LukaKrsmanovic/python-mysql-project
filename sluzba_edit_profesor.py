from classes import Profesor
from extract_db_info import profesori

#potrebno je napravoti funkciju koja ce da poziva promjene u klasi Profesor u zavisnosti
#od toga sta zeli osoba da promjeni

#vjezbe radi, prvo cemo napraviti neki niz profesora
#profesori=[Profesor("Oleg", "Obradovic", "oobradovic@gmail.com"),Profesor("Jasna", "Vukalovic", "anja.vukalovic@gmail.com"),Profesor("Gojko", "Joksimovic", "gjoksimovic@gmail.com")]


def sluzba_edit_profesor():
    postoji=False
    print("Unesite email profesora sa kojim zelite da radite")
    email_profesora= input()
    for clan in profesori:
        if clan.get_email() == email_profesora:
            nas_profesor = clan
            postoji=True
    if postoji:
           
        print("Unesite da koji metod zelite da radite: 1-get,2-delete,3-update")
        metod=int(input())
        if metod == 1:
            print("Unesite koji argument zelite da dobijete:ime,prezime ili profesor email")
            argument=input()
            if argument == "ime":
                print(nas_profesor.get_ime())
            elif argument == "prezime":
                print(nas_profesor.get_prezime())
            elif argument == "profesor email":
                print(nas_profesor.get_email())
            else:
                print("Unijeti argument ne postoji")
        
        if metod == 2:
            profesori.remove(nas_profesor)
            nas_profesor.delete()
            print("Obrisan profesor!")

        if metod == 3:
            print("Unesite koji argument zelite da promjenite:ime, prezime ili profesor email")
            argument=input()
            print("Unesite novu vrijednost argumenta")
            nova_vrijednost=input()
            if argument == "ime":
                nas_profesor.set_ime(nova_vrijednost)
            elif argument == "prezime":
                nas_profesor.set_prezime(nova_vrijednost)
            elif argument == "profesor email":
                nas_profesor.set_email(nova_vrijednost)
            else:
                print("Unijeti argument ne postoji")
    else:
        print("Email profesora koji ste unijeli ne postoji u nasoj bazi. Da li biste htjeli dodati novog profesora?")
        odgovor=input()
        if odgovor=="ne":
            raise ValueError("Nemamo tog profesora u bazi!")
        else:
            #insert-potrebno je napraviti novi objekat Predmet samim tim ce se aktivirati konstruktor
            print("Unesite info o novom Profesoru u redosledu:ime,prezime i email profesora")
            ime=input()
            prezime=input()
            email=input()
            #sada imamo sve u new[i] kao string
            newprof =Profesor(ime,prezime,email)
            profesori.append(newprof)








#sluzba_edit_profesor()
