import sluzba_edit_studenti as st
import sluzba_edit_profesor as prof
import sluzba_edit_predmet as pred
from classes import Student,Predmet,Profesor


def sluzba_edit():
    izbor = int(input("Da li zelite da editujete podatke za 1-student, 2-predmet, 3-profesor"))

    if izbor == 1:
        st.sluzba_edit_studenti()

    elif izbor== 2:
        pred.sluzba_edit_predmet()

    elif izbor == 3:
        prof.sluzba_edit_profesor()

    else:
        raise ValueError("Unos mora biti 1,2 ili 3")

