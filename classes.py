import queries_lukak as q
import queriesanjav as a


class Student:
    def __init__(self, ime, prezime, JSK, broj_indeksa, \
                 godina_studija, status, balans, novi_unos = True):
        self.ime = ime
        self.prezime = prezime
        self.JSK = JSK
        self.broj_indeksa = broj_indeksa
        self.godina_studija = godina_studija
        self.status = status
        self.balans = balans

        # Dodavanje novog reda u tabeli Student
        if novi_unos:
            status_upita = q.insert_student((ime, prezime, JSK, broj_indeksa, godina_studija, status, balans))
            if status_upita == 1:
                print("Uspjesno unijet novi student")
            else:
                print("Nije u redu\n",status_upita)

    # Funkcija da objedini sve settere
    def set_vrijednost(self, polje, vrijednost, broj_indeksa):
        status_upita = q.update_student(polje, vrijednost, broj_indeksa)
        if status_upita == 1:
            print(f"\nPolje {polje} studenta je pormijenjeno na: {vrijednost}")
        elif status_upita == 0:
            print(f"\nPolje {polje} studenta je vec postavljeno na \
                     vrijednost {vrijednost}!\nNije doslo do izmjene")
            return False
        return True

    #   === Setters ===   #

    def set_ime(self, ime):
        uspjesno = self.set_vrijednost("ime", ime, self.broj_indeksa)
        if uspjesno:
            self.ime = ime

    def set_prezime(self, prezime):
        uspjesno = self.set_vrijednost("prezime", prezime, self.broj_indeksa)
        if uspjesno:
            self.prezime = prezime

    def set_JSK(self, JSK):
        uspjesno = self.set_vrijednost("JSK", JSK, self.broj_indeksa)
        if uspjesno:
            self.JSK = JSK

    def set_broj_indeksa(self, broj_indeksa):
        uspjesno = self.set_vrijednost("broj_indeks", broj_indeksa, self.broj_indeksa)
        if uspjesno:
            self.broj_indeksa = broj_indeksa

    def set_godina_studija(self, godina_studija):
        uspjesno = self.set_vrijednost("godina_studija", godina_studija, self.broj_indeksa)
        if uspjesno:
            self.godina_studija = godina_studija

    def set_status(self, status):
        uspjesno = self.set_vrijednost("status_stud", status, self.broj_indeksa)
        if uspjesno:
            self.status = status

    def set_balans(self, balans):
        uspjesno = self.set_vrijednost("balans", balans, self.broj_indeksa)
        if uspjesno:
            self.balans = balans

    #   === Getters ===   #
        
    def get_ime(self):
        return self.ime
        
    def get_prezime(self):
        return self.prezime
        
    def get_JSK(self):
        return self.JSK
        
    def get_broj_indeksa(self):
        return self.broj_indeksa
        
    def get_godina_studija(self):
        return self.godina_studija
        
    def get_status(self):
        return self.status
        
    def get_balans(self):
        return self.balans

    # Delete metod
    def delete(self):
        status_upita = q.delete_student(self.broj_indeksa)
        if status_upita == 1:
            print(f"\nStudent ciji je indeks = {self.broj_indeksa} je obrisan iz tabele Student")
        elif status_upita == 0:
            print(f"\nStudent ciji je indeks = {self.broj_indeksa} ne postoji u bazi!\nNema promjena")


class Profesor:
    def __init__(self, ime, prezime, email, novi_unos = True):
        self.ime = ime
        self.prezime = prezime
        self.email = email

        #dodavanje novog reda u tabeli Profesor-dodavanje novog profesora
        if novi_unos:
            status_upita = a.insert_profesor((ime, prezime, email))
            if status_upita == 1:
                print("Uspjesno unijet novi profesor")
            else:
                print("Nije u redu\n",status_upita)

     #getteri

    def get_ime(self):
        return self.ime

    def get_prezime(self):
        return self.prezime

    def get_email(self):
        return self.email
    
    #setteri
    def set_vrijednost(self, polje, vrijednost, email):
        status_upita = a.update_profesor(polje, vrijednost, email)
        if status_upita == 1:
            print(f"\nPolje {polje} profesora je pormijenjeno na: {vrijednost}")
        elif status_upita == 0:
            print(f"\nPolje {polje} profesora je vec postavljeno na \
                     vrijednost {vrijednost}!\nNije doslo do izmjene")
            return False
        return True



    def set_ime(self, ime):
        uspjesno = self.set_vrijednost("ime", ime, self.email)
        if uspjesno:
            self.ime = ime

    def set_prezime(self, prezime):
        uspjesno = self.set_vrijednost("prezime", prezime, self.email)
        if uspjesno:
            self.prezime = prezime

    def set_email(self, email):
        uspjesno = self.set_vrijednost("email", email, self.email)
        if uspjesno:
            self.email = email
    
    #delete 
    def delete(self):
        status_upita = a.delete_profesor(self.email)
        if status_upita == 1:
            print(f"\nProfesor ciji je email = {self.email} je obrisan iz tabele Profesor")
        elif status_upita == 0:
            print(f"\nProfesor ciji je email = {self.email} ne postoji u bazi!\nNema promjena")


class Predmet:
    def __init__(self, ime, opis, broj_kredita, \
                 godina_studija, profesor_email, novi_unos = True):
        self.ime = ime
        self.opis = opis
        self.broj_kredita = broj_kredita
        self.godina_studija = godina_studija
        self.profesor_id = a.find_profesor_id(profesor_email)

        #dodavanje novog reda u tabeli Predmet
        if novi_unos:
            status_upita = a.insert_predmet((ime,opis,broj_kredita,godina_studija,profesor_email))
            if status_upita == 1:
                print("Uspjesno unijet novi predmet")
            else:
                print("Nije u redu\n",status_upita)




   #getteri

    def get_ime(self):
        return self.ime

    def get_opis(self):
        return self.opis

    def get_broj_kredita(self):
        return self.broj_kredita

    def get_godina_studija(self):
        return self.godina_studija

    def get_profesor_id(self):
        return self.profesor_id
    
    #setter
    

    def set_vrijednost(self, polje, vrijednost, ime):
        status_upita = a.update_predmet(polje, vrijednost, ime)
        if status_upita == 1:
            print(f"\nPolje {polje} predmeta je promijenjeno na: {vrijednost}")
        elif status_upita == 0:
            print(f"\nPolje {polje} predmeta je vec postavljeno na \
                     vrijednost {vrijednost}!\nNije doslo do izmjene")
            return False
        return True

    def set_ime(self,ime):
        uspjesno=self.set_vrijednost("ime", ime, self.ime)
        if uspjesno:
            self.ime=ime
    
    def set_opis(self,opis):
        uspjesno=self.set_vrijednost("opis",opis,self.ime)
        if uspjesno:
            self.opis=opis
    
    def set_broj_kredita(self,broj_kredita):
        uspjesno=self.set_vrijednost("broj_kredita",broj_kredita,self.ime)
        if uspjesno:
            self.broj_kredita=broj_kredita
    
    def set_profesor_id(self,profesor_email):
        uspjesno=self.set_vrijednost("profesor_id",a.find_profesor_id(profesor_email),self.ime)
        if uspjesno:
            self.profesor_id=a.find_profesor_id(profesor_email)
    
    def set_godina_studija(self,godina_studija):
        uspjesno=self.set_vrijednost("godina_studija",godina_studija,self.ime)
        if uspjesno:
            self.godina_studija=godina_studija

    #delete predmet

    def delete(self):
        status_upita=a.delete_predmet(self.ime)
        if status_upita == 1:
            print(f"\nPredmet cije je ime = {self.ime} je obrisan iz tabele Predmet")
        elif status_upita == 0:
            print(f"\nPredmet cije je ime = {self.ime} ne postoji u bazi!\nNema promjena")

    
    
    

class Sluzba:
    def __init__(self, ime, email, sifra, novi_unos = True):
        self.ime = ime
        self.email = email
        self.sifra = sifra

        #dodavanje novog reda u tabeli Sluzba-novi radnik sluzbe
        if novi_unos:
            status_upita = a.insert_sluzba((ime,email,sifra))
            if status_upita == 1:
                print("Uspjesno unijet novi radnik sluzbe")
            else:
                print("Nije u redu\n",status_upita)

    #getteri dodati za Sluzbu
    def get_ime(self):
        return self.ime

    def get_email(self):
        return self.email

    def get_sifra(self):
        return self.sifra
    
    #setteri dodati za Sluzbu
    def set_vrijednost(self, polje, vrijednost, email):
        status_upita = a.update_sluzba(polje, vrijednost, email)
        if status_upita == 1:
            print(f"\nPolje {polje} radnika sluzbe je promijenjeno na: {vrijednost}")
        elif status_upita == 0:
            print(f"\nPolje {polje} radnika sluzbe je vec postavljeno na \
                     vrijednost {vrijednost}!\nNije doslo do izmjene")
            return False
        return True

    def set_ime(self,ime):
        uspjesno=self.set_vrijednost("ime", ime, self.email)
        if uspjesno:
            self.ime=ime
    
    def set_email(self,email):
        uspjesno=self.set_vrijednost("email",email,self.email)
        if uspjesno:
            self.email=email
    
    def set_sifra(self,sifra):
        uspjesno=self.set_vrijednost("sifra",sifra,self.email)
        if uspjesno:
            self.sifra=sifra
    

    #delete radnika sluzbe
    def delete(self):
        status_upita=a.delete_sluzba(self.email)
        if status_upita == 1:
            print(f"\nRadnik sluzbe ciji je email = {self.email} je obrisan iz tabele Sluzba")
        elif status_upita == 0:
            print(f"\nRadnik sluzbe ciji je email = {self.email} ne postoji u bazi!\nNema promjena")


class Upis:
    def __init__(self, skolska_godina, u_toku, novi_unos = True):
        self.skolska_godina = skolska_godina
        self.u_toku = u_toku

        # Dodavanje novog upisa
        if novi_unos:
            pass

    def get_skolska_godina(self):
        return self.skolska_godina

    def get_u_toku(self):
        return self.u_toku


class Student_predmet_upis:
    def __init__(self, student_id, predmet_id, upis_id, polozen, novi_unos = True):
        self.student_id = student_id
        self.predmet_id = predmet_id
        self.upis_id = upis_id
        self.polozen = polozen

        # Dodavanje novog podatka o polozenim predmetima studenta
        if novi_unos:
            status_upita = q.insert_student_predmet_upis(student_id, predmet_id, upis_id, polozen)
            if status_upita == 1:
                print("Uspjesno unijeti podaci o predmetu koji student slusa sada")
            else:
                print("Nije u redu\n",status_upita)

    def get_student_id(self):
        return self.student_id

    def get_predmet_id(self):
        return self.predmet_id

    def get_upis_id(self):
        return self.upis_id

    def get_polozen(self):
        return self.polozen
