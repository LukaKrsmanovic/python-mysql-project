from classes import Student, Student_predmet_upis
from extract_db_info import studenti, upisi, studenti_predmet_upis
import queries_lukak as q


def prenos(student):
    student.set_status('Samofinansiranje')
    student.set_balans('0')

def upisi_studenta(student):
    ukupno_kredita = 0

    nepolozeni_predmeti = q.nepolozeni_predmeti(
        student.get_broj_indeksa(), 
        student.get_godina_studija()
    )
    # U slcaju da je doslo do greske nepolozeni predmeti ce biti Exception
    if type(nepolozeni_predmeti) != type([]):
        return -1

    print("Nepolozeni predmeti sa prethodnih godina:")
    print("** Pravilo: Predmet --> broj kredita")
    for predmet in nepolozeni_predmeti:
        print(f"- {predmet['ime']} --> {predmet['broj_kredita']}")
        student_predmet_upis = Student_predmet_upis(
            q.student_id(student.get_broj_indeksa()),
            q.predmet_id(predmet['ime']),
            q.upis_id(),
            0
        )
        studenti_predmet_upis.append(student_predmet_upis)
        ukupno_kredita += predmet['broj_kredita']

    prenio_kredita = ukupno_kredita

    novi_predmeti = q.novi_predmeti(student.get_godina_studija()+1)
    if type(novi_predmeti) != type([]):
        return -1

    print(f"\nPredmeti za {student.get_godina_studija()+1}. godinu studija su:")
    print("** Pravilo: Predmet --> broj kredita")
    for predmet in novi_predmeti:
        print(f"- {predmet['ime']} --> {predmet['broj_kredita']}")

    print("\nPredmeti sa prethodnih godina su vec dodati, treba odabrati nove predmete.")
    print(f"Preostalo je {30 - ukupno_kredita} kredita za odabrati\n")

    # Ako student zeli da vidi opis nekog predmeta
    while(True):
        odgovor = input("Da li zelite opis o nekom od novih predmeta? [Y/N]: ")
        print()
        if odgovor == 'N':
            break
        odgovor = input("Unesite naziv predmeta o kojem zelite opis: ")
        predmet = list(filter(lambda novi_predmet : odgovor == novi_predmet['ime'], novi_predmeti))[0]
        print(predmet['opis'] if predmet['opis'] != "" else "Ovaj predmet nema opis :(")
        print()
    
    poruka = f"Unesite nazive predmeta koje zelite slusati (do {30 - ukupno_kredita} kredita) \
                     i pazite da budu odvojeni zarezom i prazninom \
                     odmah poslije zareza (npr. Predmet1, Predmet2, Predmet3)\n"
    odgovor = input(poruka)
    odabrani_predmeti = set(odgovor.split(", "))
    odabrani_predmeti = list(filter(lambda predmet : predmet['ime'] in odabrani_predmeti, novi_predmeti))

    predmeti_za_upisati = []
    for predmet in odabrani_predmeti:
        novo_ukupno = ukupno_kredita + predmet['broj_kredita']
        if novo_ukupno <= 30:
            predmeti_za_upisati.append(predmet)
            ukupno_kredita = novo_ukupno
        else:
            novo_ukupno = ukupno_kredita
    
    # Ovdje se radi upis odabranih predmeta ovog studenta u tabelu student_predmet_upis
    print("\nNovi predmeti koje treba upisati u bazu su: ")
    for predmet in predmeti_za_upisati:
        print(f"- {predmet['ime']} --> {predmet['broj_kredita']}")
        student_predmet_upis = Student_predmet_upis(
            q.student_id(student.get_broj_indeksa()),
            q.predmet_id(predmet['ime']),
            q.upis_id(),
            0
        )
        studenti_predmet_upis.append(student_predmet_upis)

    print("\nSvi predmeti (stariji i noviji) su upisani u bazu!")

    # Ako je odabrao makar jedan predmet sa nove godine da slusa
    if len(predmeti_za_upisati) != 0:
        student.set_godina_studija(student.get_godina_studija() + 1)

    if prenio_kredita > 10:
        prenos(student)

    print("\nPodaci o studentu su update-ovani")

    return 1
    




    