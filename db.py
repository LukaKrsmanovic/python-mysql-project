
from passwords_file import sifra

import pymysql.cursors
import csv  
import asyncio


"""
cursor.execte(query) vraca integer ako nije doslo do greske
  - za select vraca broj redova tabele
  - za insert, update... vraca:
        1 ako se desila izmjena u tabeli (ako se jedna stvar promijenila u tabeli)
        0 ako se nista nije promijenilo u tabeli 
"""

def execute_query(query_type, query, db_name = 'fakultet2'):
    # Zapocni konekciju
    connection = pymysql.connect(host = 'localhost',
                                user = 'root',
                                password = sifra,
                                database = db_name,
                                charset = 'utf8mb4',
                                cursorclass = pymysql.cursors.DictCursor)
    
    # Pomocne promjenjive
    rezultat = []
    select_upit = (query_type == "select")

    # Izvrsi upit i zatvori konekciju
    with connection:
        with connection.cursor() as cursor:
            status = cursor.execute(query)
            if select_upit:
                rezultat = cursor.fetchall()
        
        if not(select_upit):
            connection.commit()

    return [status, rezultat]


def select(query):
    return execute_query("select", query)

def insert(query):
    return execute_query("insert", query)

def update(query):
    return execute_query("update", query)

def delete_row(query):
    return execute_query("delete", query)

def delete_table(query):
    return execute_query("drop", query)



# Tesing queries (delete later)

_, redovi = select('select * from Student')
# print(redovi)
for red in redovi:
    print(red)


#status, _ = insert('insert into Student values (3, "Luka", "Prezime", "QJKB", "13/21", 3, "Budzet", "475");')

# print(status)
# if status == 0:
#     print("Nista za brisanje, tabela nije promijenjena")
# else:
#     print("Uspjesno su unijeli novog!")



import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders

async def sendFile():
   
    fromaddr = "mrakcevic@gmail.com"
    toaddr = "mrakcevic@gmail.com"
   
    # instance of MIMEMultipart
    msg = MIMEMultipart()
# storing the senders email address  
    msg['From'] = fromaddr
# storing the receivers email address 
    msg['To'] = toaddr
# storing the subject 
    msg['Subject'] = "Subject of the Mail"
# string to store the body of the mail
    body = "Body_of_the_mail"
# attach the body with the msg instance
    msg.attach(MIMEText(body, 'plain'))
# open the file to be sent 
    filename = "sluzba_upis.csv"
    attachment = open("sluzba_upis.csv", "rb")
# instance of MIMEBase and named as p
    p = MIMEBase('application', 'octet-stream')
# To change the payload into encoded form
    p.set_payload((attachment).read())
# encode into base64
    encoders.encode_base64(p)
    p.add_header('Content-Disposition', "attachment; filename= %s" % filename)
# attach the instance 'p' to instance 'msg'
    msg.attach(p)
# creates SMTP session
    s = smtplib.SMTP('smtp.gmail.com', 587)
# start TLS for security
    s.starttls()
# Authentication
    s.login(fromaddr, password_email)
# Converts the Multipart msg into a string
    text = msg.as_string()
# sending the mail
    s.sendmail(fromaddr, toaddr, text)
# terminating the session
    s.quit()
    await asyncio.sleep(1)




with open('sluzba_upis.csv', 'w', encoding='UTF8') as f:
    writer = csv.writer(f)








