
from classes import Student
from extract_db_info import studenti



def sluzba_edit_studenti():
    
    metod = input("Izaberite get,insert,delete ili update: ")
    if metod not in(["get","insert","delete","update"]):
        raise ValueError("Ta funkcija ne postoji")
    
    if metod == "get" or metod == "delete" or metod == "update":
        indeks_studenta = input("Unesite indeks studenta: ")
        student = ""
        for row in range(len(studenti)):
            if studenti[row].broj_indeksa == indeks_studenta:
                student = studenti[row]
                break
        if student == "":
            raise ValueError("Indeks ne pripada nijednom studentu")

        

        if metod == "get":
            podatak = input("Unesite kopjem podatku zelite da pristupite: ") 
            if podatak  not in["ime", "prezime","JSK","godina studija", "status", "balans","broj indeksa"]:
                raise ValueError("Ne posjedujemo te podatatke")
            if podatak == "ime":
                print(student.get_ime())
            if podatak == "prezime":
                print(student.get_prezime())
            if podatak == "JSK":
                print(student.get_JSK())
            if podatak == "godina studija":
                print(student.get_godina_studija())
            if podatak == "broj indeksa":
                print(student.get_broj_indeksa())
            if podatak == "status":
                print(student.get_status())
            if podatak == "balans":
                print(student.get_balans())

        if metod == "delete":

            studenti.remove(student)
            student.delete()


        if metod == "update":
            podatak = input("Unesite kopjem podatku zelite da pristupite: ") 
            if podatak  not in["ime", "prezime","JSK","godina studija", "status", "balans","broj indeksa"]:
                raise ValueError("Ne posjedujemo te podatatke")
            naziv = input("Unesite novu vrijednost podatka: ")
            if podatak == "ime":
                student.set_ime(naziv)
            if podatak == "prezime":
                student.set_prezime(naziv)
            if podatak== "JSK":
                student.set_JSK(naziv)
            if podatak == "godina studija":
                student.set_godina_studija(naziv)
            if podatak == "broj indeksa":
                student.set_broj_indeksa(naziv)
            if podatak == "status":
                student.set_status(naziv)
            if podatak == "balans":
                student.set_balans(naziv)



        
    if metod == "insert":
        new_ime = input("unesi ime: ")
        new_prezime = input("unesi prezime: ")
        new_JSK = input("unesi JSK: ")
        new_godina_studija = int(input("Unesi godinun studija: ")) 
        new_broj_indeksa = input("Unesi broj indeksa: ")
        new_status = input("Unesi novi status: ")
        new_balans = input("Unesi novi balans: ")

        student = Student(new_ime,new_prezime,new_JSK,new_broj_indeksa,new_godina_studija,new_status,new_balans)

        studenti.append(student)

            
        

