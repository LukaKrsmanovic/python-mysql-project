import db

# Upit za insert u tabelu Student, podaci je tuple
def insert_student(podaci):
    if len(podaci) != 7:
        raise ValueError("Nisu unijeti svi podaci studenta!")
    try:
        status, _ = db.insert(f"insert into Student(ime, prezime, JSK, broj_indeks, godina_studija, \
                                status_stud, balans) values ('{podaci[0]}', '{podaci[1]}', '{podaci[2]}', \
                                '{podaci[3]}', {podaci[4]}, '{podaci[5]}', '{podaci[6]}');")
    except Exception as e:
        status = e
    return status

# Upit za update tabele Student
def update_student(kolona, vrijednost, broj_indeksa):
    try:
        if kolona != "godina_studija":
            vrijednost = f"'{vrijednost}'"
        status, _ = db.update(f"update Student set {kolona} = {vrijednost} where broj_indeks = '{broj_indeksa}'")
    except Exception as e:
        status = e
    return status

# Upit delete reda tabele Student
def delete_student(broj_indeksa):
    try:
        status, _ = db.delete_row(f"delete from Student where broj_indeks = '{broj_indeksa}'")
    except Exception as e:
        status = e
    return status

# Upit za dobijanje nepolozenih predmeta do sada
def nepolozeni_predmeti(indeks, godina_studija):
    upit = f"""select * from Predmet p where ime not in (
	select distinct p.ime 'Predmet' 
		from Student s, student_predmet_upis spu, Predmet p 
		where s.id = spu.student_id and spu.predmet_id = p.id 
			and s.id = (select id from Student where broj_indeks = '{indeks}') and spu.polozen = 1
	) and godina_studija between 1 and {godina_studija};"""
    redovi = []
    try:
        _, redovi = db.select(upit)
    except Exception as e:
        return e
    return redovi

# Upit za dobijanje novih predmeta (sa godine koju upisuje)
def novi_predmeti(godina_studija):
    upit = f"""select * from Predmet where godina_studija = {godina_studija};"""
    redovi = []
    try:
        _, redovi = db.select(upit)
    except Exception as e:
        return e
    return redovi

# Dobijanje student id-a
def student_id(broj_indeksa):
    upit = f"select id from Student where broj_indeks = '{broj_indeksa}'"
    redovi = []
    try:
        _, redovi = db.select(upit)
    except Exception as e:
        return e
    return redovi[0]['id']

# Dobijanje predmet id-a
def predmet_id(ime):
    upit = f"select id from Predmet where ime = '{ime}'"
    redovi = []
    try:
        _, redovi = db.select(upit)
    except Exception as e:
        return e
    return redovi[0]['id']

# Dobijanje upis id-a
def upis_id():
    upit = f"select id from Upis where u_toku = 1"
    redovi = []
    try:
        _, redovi = db.select(upit)
    except Exception as e:
        return e
    return redovi[0]['id']

# Dodavanje novog reda u tabelu student_predmet_upis
def insert_student_predmet_upis(student_id, predmet_id, upis_id, polozen):
    upit = f"insert into student_predmet_upis values ({student_id}, {predmet_id}, {upis_id}, {polozen})"
    try:
        status, _ = db.insert(upit)
    except Exception as e:
        status = e
    return status
