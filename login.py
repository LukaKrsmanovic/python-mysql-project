from classes import Student, Sluzba
from extract_db_info import studenti, radnici
import sluzba_edit
import functions
from upis_studenata import upisi_studenta
import asyncio

radnici_sluzbe = radnici

uloga = input("Da li ste radnik sluzbe ili student: ")
provjera_JSK = ""
provjera_email = ""
provjera_sifra = ""

if uloga == "radnik sluzbe":
    provjera_email = input("Unesite email adresu: ")

    for row in radnici_sluzbe:
        if row.email == provjera_email:
            break
    if provjera_email != row.email:
        raise ValueError("Nepostojeci email")
    
    provjera_sifra = input("Unesi sifru: ")

    if row.sifra == provjera_sifra:
        print(f"Welcome {row.ime}")

    else:
        raise ValueError("Pogresna sifra")
    

    svrha_logina = input("Da li zelite izvsiti edit ili upis: ")
    
    if svrha_logina == "edit":
        sluzba_edit.sluzba_edit()
    elif svrha_logina == "upis":
        asyncio.run(functions.sluzba_upis())
    else:
        raise ValueError ("Nepostojece svrha")
    

    


elif uloga == "student":
    provjera_JSK = input("Unesite JSK: ")

    for row in studenti:
        if row.JSK == provjera_JSK:
            print(f"Welcome {row.ime}")
            upisi_studenta(row)
            break
    if provjera_JSK != row.JSK:
        raise ValueError("Nepostojeci JSK")








else:
    raise ValueError("Unos mora biti radnik sluzbe ili student") 