from passwords_file import sifra
import pymysql.cursors
import csv  
import asyncio
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders
import db

async def sendFile(filename):
   
    fromaddr = "mrakcevic@gmail.com"
    toaddr = "mrakcevic@gmail.com"
   
    # instance of MIMEMultipart
    msg = MIMEMultipart()
# storing the senders email address  
    msg['From'] = fromaddr
# storing the receivers email address 
    msg['To'] = toaddr
# storing the subject 
    msg['Subject'] = "Subject of the Mail"
# string to store the body of the mail
    body = "Lista predmeta"
# attach the body with the msg instance
    msg.attach(MIMEText(body, 'plain'))
# open the file to be sent 
    # filename = "sluzba_upis.csv"
    attachment = open(filename, "rb")
# instance of MIMEBase and named as p
    p = MIMEBase('application', 'octet-stream')
# To change the payload into encoded form
    p.set_payload((attachment).read())
# encode into base64
    encoders.encode_base64(p)
    p.add_header('Content-Disposition', "attachment; filename= %s" % filename)
# attach the instance 'p' to instance 'msg'
    msg.attach(p)
# creates SMTP session
    s = smtplib.SMTP('smtp.gmail.com', 587)
# start TLS for security
    s.starttls()
# Authentication
    s.login(fromaddr, "ludakludi")
# Converts the Multipart msg into a string
    text = msg.as_string()
# sending the mail
    s.sendmail(fromaddr, toaddr, text)
# terminating the session
    s.quit()
    await asyncio.sleep(1)




def obavijesti_profesora():
    prof_email = input("Unesite email profesora kojeg zelite da obavijesite: ")
    prof_query = db.select(f"""select prof.ime 'Ime profesora', pred.ime 'Predmet', concat(s.ime, ' ', s.prezime) as 'Student', s.broj_indeks 'Indeks'
	from Predmet pred, student_predmet_upis spu, Profesor prof, Student s, Upis u
    where prof.id = (select id from Profesor where email = '{prof_email}') and
		  pred.profesor_id = prof.id and
          pred.id = spu.predmet_id and
          spu.upis_id = u.id and
          year(u.skolska_godina) = 2021 and
          spu.student_id = s.id """)
    with open('lista_predmeta.csv', 'w', encoding='UTF8') as f:
        writer = csv.writer(f)
        writer.writerow(prof_query)
        asyncio.create_task(sendFile("lista_predmeta.csv"))    
    


async def sluzba_upis():
    school_year = input("Unesite skolsku godinu: ")
    # vraca studenta za tu skolsku godinu
    status2, year = db.select(f"select distinct s.JSK, year(u.skolska_godina) from Student s, student_predmet_upis spu, Upis u where u.id = spu.upis_id and spu.student_id = s.id and year(u.skolska_godina) = '{school_year}'")
    with open('sluzba_upis.csv', 'w', encoding='UTF8') as f:
        writer = csv.writer(f)
        writer.writerow(year)
        asyncio.create_task(sendFile("sluzba_upis.csv"))
        enrollment  = db.select("select u_toku from Upis ORDER BY id desc LIMIT 1")
        new_enrollment  = []
        new_enrollment  = enrollment[1][0]['u_toku']
        if new_enrollment == 1:
            print("Upis je u toku")
            obavijesti_profesora()
    if status2 != 2:
        print("U izabranoj godini nema upisanih studenata, ne zaboravite da je fakultet nov")  







