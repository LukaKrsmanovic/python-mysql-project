from classes import Predmet
from extract_db_info import predmeti

#potrebno je napravoti funkciju koja ce da poziva promjene u klasi Predmet u zavisnosti
#od toga sta zeli osoba da promjeni

#vjezbe radi, prvo cemo napraviti neki niz predmeta


def sluzba_edit_predmet():

    postoji=False
    print("Unesite naziv predmeta sa kojim zelite da radite")
    naziv_predmeta = input()
    for clan in predmeti:
        if clan.get_ime() == naziv_predmeta:
            nas_predmet = clan
            postoji=True
    if postoji:
        print("Unesite koji metod zelite da radite: 1-get,2-delete,3-update: ")
        metod=int(input())
        if metod == 1:
            print("Unesite koji argument zelite da dobijete: opis,broj kredita,godina studija, profesor email\n")
            argument=input()
            if argument == "opis":
                print(nas_predmet.get_opis())
            elif argument == "broj kredita":
                print(nas_predmet.get_broj_kredita())
            elif argument == "godina studija":
                print(nas_predmet.get_godina_studija())
            elif argument == "profesor email":
                print(nas_predmet.get_profesor_id())
            else:
                print("Unijeti argument ne postoji")
        
        if metod == 2:
            predmeti.remove(nas_predmet)
            nas_predmet.delete()
            print("Predmet obrisan!")

        if metod == 3:
            print("Unesite koji argument zelite da promjenite: ")
            argument=input()
            print("Unesite novu vrijednost argumenta: ")
            nova_vrijednost=input()
            if argument == "opis":
                nas_predmet.set_opis(nova_vrijednost)
            elif argument == "broj kredita":
                nas_predmet.set_broj_kredita(nova_vrijednost)
            elif argument == "godina studija":
                nas_predmet.set_godina_studija(nova_vrijednost)
            elif argument == "profesor email":
                nas_predmet.set_profesor_id(nova_vrijednost)
            elif argument =="ime":
                nas_predmet.set_ime(nova_vrijednost)
            else:
                print("Unijeti argument ne postoji")

    else:
        print("Naziv predmeta koji ste unijeli ne postoji u nasoj bazi. Da li biste htjeli dodati novi predmet?")
        odgovor=input()
        if odgovor=="ne":
            raise ValueError("Nemamo taj predmet u bazi!")
        else:
            #insert-potrebno je napraviti novi objekat Predmet samim tim ce se aktivirati konstruktor
            print("Unesite info o novom Predmetu u redosledu:ime,opis,broj kredita,godina studija, email profesora koji predaje predmet")
            #sada imamo sve u new[i] kao string
            ime=input()
            opis=input()
            brojkredita=input()
            godinastudija=input()
            emailprof=input()
            s=Predmet(ime,opis,brojkredita,godinastudija,emailprof)
            predmeti.append(s)








#sluzba_edit_predmet()
