import db
from classes import Student, Predmet, Profesor, Upis, Student_predmet_upis, Sluzba

def printaj(obj):
    print(f"Ime: {obj.ime}, Prezime: {obj.prezime}, Indeks: {obj.broj_indeksa}")


#  ===  Za studente  ===  #

_, redovi = db.select("select * from Student")

studenti = []
for red in redovi:
    student = Student(red['ime'], red['prezime'], red['JSK'], red['broj_indeks'], \
                      red['godina_studija'], red['status_stud'], red['balans'], False)
    studenti.append(student)


#  ===  Za predmete  ===  #

_, redovi = db.select("select * from Predmet")

predmeti = []
for red in redovi:
    _, pomocni = db.select(f"select distinct prof.email from Predmet pred, Profesor prof \
                             where pred.profesor_id = prof.id and pred.id = {red['id']};")
    predmet = Predmet(
        red['ime'], 
        red['opis'], 
        red['broj_kredita'], 
        red['godina_studija'], 
        pomocni[0]['email'], 
        False
    )
    predmeti.append(predmet)


#  ===  Za profesore  ===  #

_, redovi = db.select("select * from Profesor")

profesori = []
for red in redovi:
    profesor = Profesor(red['ime'], red['prezime'], red['email'], False)
    profesori.append(profesor)


#  ===  Za upise  ===  #

_, redovi = db.select("select * from Upis")

upisi = []
for red in redovi:
    upis = Upis(red['skolska_godina'], red['u_toku'], False)
    upisi.append(upis)


#  ===  Za randike sluzbe  ===  #

_, redovi = db.select("select * from Sluzba")

radnici = []
for red in redovi:
    radnik = Sluzba(red['ime'], red['email'], red['sifra'], False)
    radnici.append(radnik)


#  ===  Za informacije upisa studenta  ===  #

_, redovi = db.select("select * from student_predmet_upis")

studenti_predmet_upis = []
for red in redovi:
    student_predmet_upis = Student_predmet_upis(
        red['student_id'], 
        red['predmet_id'], 
        red['upis_id'], 
        red['polozen'], 
        False
    )
    studenti_predmet_upis.append(student_predmet_upis)
