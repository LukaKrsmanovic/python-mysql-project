import db
import re

# Upit za insert u tabelu Predmet, podaci je tuple
def insert_predmet(podaci):
    if len(podaci) != 5:
        raise ValueError("Nisu unijeti svi podaci predmeta!")
    try:
        status, _ = db.insert(f"insert into Predmet(ime, opis, broj_kredita, godina_studija, \
                                profesor_email ) values ('{podaci[0]}', '{podaci[1]}', {podaci[2]}, \
                                {podaci[3]}, (select id from Profesor where email = '{podaci[4]}'));")
    except Exception as e:
        status = e
    return status

# Upit za update tabele Predmet
def update_predmet(kolona, vrijednost, ime):
    try:
        if kolona != "broj_kredita" and kolona != "godina_studija":
            vrijednost = f"'{vrijednost}'"
        status, _ = db.update(f"update Predmet set {kolona} = {vrijednost} where ime = '{ime}'")
    except Exception as e:
        status = e
    return status

# Upit delete reda tabele Predmet:
def delete_predmet(ime):
    try:
        status, _ = db.delete_row(f"delete from Predmet where ime = '{ime}'")
    except Exception as e:
        status = e
    return status


# Upit za insert u tabelu Profesori, podaci je tuple
def insert_profesor(podaci):
    if len(podaci) != 3:
        raise ValueError("Nisu unijeti svi podaci profesora!")
    #potrebno je validirati email adresu profesora
    if re.search("^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$", podaci[2]):
        try:
            status, _ = db.insert(f"insert into Profesor(ime, prezime, email) values ('{podaci[0]}', '{podaci[1]}', '{podaci[2]}');")
        except Exception as e:
            status = e
        return status
    else:
        raise ValueError("Profesor kojeg pokusavate da unesete nema validnu email adresu!")

# Upit za update tabele Profesor
def update_profesor(kolona, vrijednost, email):
    if kolona == "email":
         if re.search("^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$", vrijednost):
            try:
                vrijednost = f"'{vrijednost}'"
                status, _ = db.update(f"update Profesor set {kolona} = {vrijednost} where email = '{email}'")
            except Exception as e:
                status = e
            return status
         else:
             raise ValueError("Nova email adresa nije validna!")
    else:
        try:
            vrijednost = f"'{vrijednost}'"
            status, _ = db.update(f"update Profesor set {kolona} = {vrijednost} where email = '{email}'")
        except Exception as e:
            status = e
        return status


# Upit delete reda tabele Profesor:
def delete_profesor(email):
    try:
        status, _ = db.delete_row(f"delete from Profesor where email = '{email}'")
    except Exception as e:
        status = e
    return status


# Upit za insert u tabelu Sluzba, podaci je tuple
def insert_sluzba(podaci):
    if len(podaci) != 3:
        raise ValueError("Nisu unijeti svi podaci radnika sluzbe!")
    #potrebno je validirati email adresu i sifru pri unosu radnika sluzbe
    if re.search("^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$", podaci[1]) and re.search("^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$",podaci[2]):
        #ako je mejl validan i sifra ima min 8 karaktera,makar jedno slovo i broj i 1 specijalni karakter
        #mozemo unijeti novog radnika sluzbe
        try:
            status, _ = db.insert(f"insert into Sluzba(ime, email, sifra) values ('{podaci[0]}', '{podaci[1]}', '{podaci[2]}');")
        except Exception as e:
            status = e
        return status
    else:
        raise ValueError("Email adresa i sifra koju ste unijeli nije validna!")

# Upit za update tabele SLuzba
def update_sluzba(kolona, vrijednost, email):
    if kolona == "email":
        if re.search("^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$", vrijednost):
            try:
                vrijednost = f"'{vrijednost}'"
                status, _ = db.update(f"update Sluzba set {kolona} = {vrijednost} where email = '{email}'")
            except Exception as e:
                status = e
            return status
        else:
            raise ValueError("Nova email adresa nije validna!")
    else:
        try:
            vrijednost = f"'{vrijednost}'"
            status, _ = db.update(f"update Sluzba set {kolona} = {vrijednost} where email = '{email}'")
        except Exception as e:
            status = e
        return status

# Upit delete reda tabele Sluzba:
def delete_sluzba(email):
    try:
        status, _ = db.delete_row(f"delete from Sluzba where email = '{email}'")
    except Exception as e:
        status = e
    return status

#Upit da nadjemo id profesora preko njegovog mejla:
def find_profesor_id(email):
    upit=f"select id from Profesor where email = '{email}'"
    redovi = []
    try:
        _, redovi = db.select(upit)
    except Exception as e:
        return e
    return redovi[0]['id']



