import queries_test as q

class Student:
    def __init__(self, id, ime, prezime, JSK, broj_indeksa, \
                 godina_studija, status, balans, novi_unos = False):
        self.id = id
        self.ime = ime
        self.prezime = prezime
        self.JSK = JSK
        self.broj_indeksa = broj_indeksa
        self.godina_studija = godina_studija
        self.status = status
        self.balans = balans
        if novi_unos:
            status_upita = q.insert_student((id, ime, prezime, JSK, broj_indeksa, godina_studija, status, balans))
            if status_upita == 1:
                print("Uspjesno unijet novi student")
            else:
                print("Nije u redu\n",status_upita)

    def set_ime(self, ime):
        status_upita = q.update_student("ime", ime, self.id)
                
    def get_id(self):
        return self.id
        
    def get_ime(self):
        return self.ime
        
    def get_prezime(self):
        return self.prezime
        
    def get_JSK(self):
        return self.JSK
        
    def get_broj_indeksa(self):
        return self.broj_indeksa
        
    def get_godina_studija(self):
        return self.godina_studija
        
    def get_status(self):
        return self.status
        
    def get_balans(self):
        return self.balans